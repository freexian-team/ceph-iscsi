ceph-iscsi (3.9-1) sid; urgency=medium

  * QA upload.
  * New upstream version 3.9
  * Remove dependency on python3-mock
  * Use dh-sequence-python3

 -- Alexandre Detiste <tchet@debian.org>  Mon, 04 Nov 2024 21:07:15 +0100

ceph-iscsi (3.6-3) sid; urgency=medium

  * QA upload.
  * Drop dependencies on python3-distutils.

 -- Matthias Klose <doko@debian.org>  Sat, 09 Mar 2024 12:03:16 +0100

ceph-iscsi (3.6-2) unstable; urgency=medium

  * QA upload.
  * Let systemd.pc decide place for systemd system unit files

 -- Chris Hofstaedtler <zeha@debian.org>  Sat, 09 Dec 2023 13:39:12 +0100

ceph-iscsi (3.6-1) unstable; urgency=medium

  * QA upload.

  [ Leandro Cunha ]
  * New upstream version.
  * Set upstream metadata fields: Changelog.
  * debian/control:
    - Bump Standards-Version to 4.6.2.
    - Added 'Rules-Requires-Root: no' in source stanza.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 12 to 13.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Leandro Cunha <leandrocunha016@gmail.com>  Sun, 22 Oct 2023 15:01:04 -0300

ceph-iscsi (3.5-3) unstable; urgency=medium

  * QA upload.
  * Ship systemd units under /lib/systemd/system so that they can get
    picked up by dh_installsystemd (Closes: #1034235).

 -- Cyril Brulebois <kibi@debian.org>  Tue, 25 Apr 2023 15:26:14 +0000

ceph-iscsi (3.5-2) unstable; urgency=medium

  * QA upload.
  * Apply patch to Test enablement, misc tidy actions - thanks to
    James Page (Closes: #945777)
  * Bump standards version

 -- Neil Williams <codehelp@debian.org>  Fri, 24 Sep 2021 13:45:45 +0100

ceph-iscsi (3.5-1) experimental; urgency=medium

  [ Sébastien Delafond ]
  * New upstream version 3.5

 -- Sebastien Delafond <seb@debian.org>  Mon, 12 Apr 2021 11:29:26 +0200

ceph-iscsi (3.4-1) unstable; urgency=medium

  [ Sébastien Delafond ]
  * New upstream version 3.4
  * Bump-up Standards-Version

 -- Sebastien Delafond <seb@debian.org>  Wed, 20 Jan 2021 10:34:01 +0100

ceph-iscsi (3.3-1) unstable; urgency=medium

  * Team upload
  * New upstream version 3.3
  * Move git repository to "debian" namespace
  * Bump Standards-Version to 4.4.1
  * Switch to debhelper compat level 12

 -- Raphaël Hertzog <raphael@freexian.com>  Mon, 21 Oct 2019 11:40:52 +0200

ceph-iscsi (3.2-1) unstable; urgency=medium

  [ Mathieu Parent ]
  * Add debian/gbp.conf
  * Add debian/watch
  * Rename package ceph-iscsi
  * Install gwcli manpage
  * Add "Debian pipeline for Developers"
  * piuparts fails because tcmu-runner is not yet in sid
  * Add init.d services
  * Replace /etc/sysconfig/ceph by /etc/default/ceph in service files
  * Fix skip-systemd-native-flag-missing-pre-depends

  [ Sébastien Delafond ]
  * Remove piuparts override now that tcmu-runner is in sid
  * First release (Closes: #933350)

 -- Sebastien Delafond <seb@debian.org>  Mon, 23 Sep 2019 08:46:44 +0200

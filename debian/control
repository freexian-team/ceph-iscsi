Source: ceph-iscsi
Section: python
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: debhelper-compat (= 13),
    dh-sequence-python3,
    pkgconf,
    python3-setuptools,
    python3-all,
    python3-configshell-fb,
    python3-cryptography,
    python3-flask,
    python3-netifaces,
    python3-openssl,
    python3-pytest,
    python3-rados,
    python3-rbd,
    python3-requests,
    python3-rtslib-fb,
    python3-setuptools,
    systemd-dev,
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://github.com/ceph/ceph-iscsi
Vcs-Browser: https://salsa.debian.org/debian/ceph-iscsi
Vcs-Git: https://salsa.debian.org/debian/ceph-iscsi.git

Package: ceph-iscsi
Architecture: all
Pre-Depends: ${misc:Pre-Depends}
Depends: python3-configshell-fb,
         python3-cryptography,
         python3-flask,
         python3-netifaces,
         python3-openssl,
         python3-rados,
         python3-rbd,
         python3-requests,
         python3-rtslib-fb,
         tcmu-runner (>= 1.4.0),
         ${misc:Depends},
         ${python3:Depends}
Description: common logic and CLI tools for creating and managing LIO gateways for Ceph
 It includes the rbd-target-api daemon which is responsible for
 restoring the state of LIO following a gateway reboot/outage and
 exporting a REST API to configure the system using tools like
 gwcli. It replaces the existing 'target' service.
 .
 There is also a second daemon rbd-target-gw which exports a REST API
 to gather statistics.
 .
 It also includes the CLI tool gwcli which can be used to configure
 and manage the Ceph iSCSI gateway, which replaces the existing
 targetcli CLI tool. This CLI tool utilizes the rbd-target-api server
 daemon to configure multiple gateways concurrently.
